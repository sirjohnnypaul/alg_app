import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { AlgorithmsModel } from '../src/business-layer/algorithms/models/alrogithms.model';
import { AlgorithmsService } from '../src/business-layer/algorithms/services/algorithms.service';

describe('AppController (e2e)', () => {
  let app: INestApplication;
  let service: AlgorithmsService;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
      providers : [AlgorithmsService]
    }).compile();
    service = moduleFixture.get<AlgorithmsService>(AlgorithmsService);
    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/')
      .expect(200)
      .expect('Hello World!');
  });

});


