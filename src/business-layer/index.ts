import { AlgorithmsModule } from "./algorithms/algorithms.module";

export const BUSINESS_LAYER_MODULES: any[] = [
  AlgorithmsModule
];
