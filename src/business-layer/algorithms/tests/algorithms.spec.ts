import { Test, TestingModule } from "@nestjs/testing";
import { AlgorithmsModel } from "../models/alrogithms.model";
import { AlgorithmsService } from "../services/algorithms.service";
import * as fs from 'fs';
import * as path from 'path';

describe('Algorithms Tests', () => {
  let service: AlgorithmsService;
  
  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      providers : [AlgorithmsService]
    }).compile();

    service = moduleFixture.get<AlgorithmsService>(AlgorithmsService);

  });

  describe('splitForBuckets', () => {
    it('should return two buckets with the correct sums', async () => {
      const elements = JSON.parse(fs.readFileSync(path.resolve(__dirname, '../../../../assets/files/dataFile.json'), 'utf-8'));
  
      const percent = 50; // Example value of 50%
      const queryData = new AlgorithmsModel.AlgorithmsQuery();
      queryData.nj = percent;

      const response = await service.handleAlgorithmLogic(queryData, elements.map(el => new AlgorithmsModel.dataDTO(el)));
      const sum = elements.reduce((acc, curr) => acc + curr.nominalAmount, 0);
      const bucket1Sum = response.basket1.elements.reduce((acc, curr) => acc + curr.nominalAmount, 0);
      const bucket2Sum = response.basket2.elements.reduce((acc, curr) => acc + curr.nominalAmount, 0);

      expect(bucket1Sum).toBeLessThanOrEqual(sum * percent);
      expect(bucket1Sum + bucket2Sum).toBe(sum);
    });
  });
});
