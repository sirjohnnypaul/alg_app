import { IsDateString, IsInt, IsNotEmpty, IsNumber, IsPositive, IsString } from 'class-validator';
import { Transform } from 'class-transformer';

export namespace AlgorithmsModel {

    export class AlgorithmsQuery {

        @Transform(({ value }) => parseInt(value))
        @IsPositive()
        @IsNotEmpty()
        @IsInt()
        nj: number;
        
      }

    export class dataDTO {

        @IsDateString()
        issueDate: Date;

        @IsDateString()
        dueDate: Date;

        @IsDateString()
        saleDate: Date;

        @IsNumber()
        nominalAmount: number;

        @IsNumber()
        actualAmount: number;

        @IsString()
        basket: string

        constructor(data?: any) {
            if(!data){
                return new dataDTO();
            }
            this.actualAmount = data.actualAmount;
            this.basket = data.basket;
            this.dueDate = data.dueDate;
            this.issueDate = data.issueDate;
            this.nominalAmount = data.nominalAmount;
            this.saleDate = data.saleDate
        }


    }

    export class basketResponseDTO {

        basketAmountSum: number;
        basketPercentageOfAll: number;
        elements: dataDTO[]

    }

    export class dataResponseDTO {

        basket1: basketResponseDTO;

        basket2: basketResponseDTO;

    }

}