import { Body, Controller, Get, Query } from "@nestjs/common";
import { AlgorithmsModel } from "../models/alrogithms.model";
import { AlgorithmsService } from "../services/algorithms.service";


@Controller('algorithms')
export class AlgorithmsController {
    constructor(
        private readonly _algorithmsService: AlgorithmsService
    ) {};

    @Get('')
    async handleAlgorithmLogic(
        @Query() query: AlgorithmsModel.AlgorithmsQuery,
        @Body() data: AlgorithmsModel.dataDTO[]
    ): Promise<AlgorithmsModel.dataResponseDTO> {
        return await this._algorithmsService.handleAlgorithmLogic(query, data)
    }
}