import { Module } from '@nestjs/common';
import { LoadBodyMiddleware } from '../../middlewares/load-body/load-body.middleware';
import { AlgorithmsController } from './controllers/algorithms.controller';
import { AlgorithmsService } from './services/algorithms.service';

@Module({
  imports: [],
  controllers: [AlgorithmsController],
  providers: [AlgorithmsService, LoadBodyMiddleware],
})
export class AlgorithmsModule {}
