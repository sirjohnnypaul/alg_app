import { Injectable } from "@nestjs/common";
import { AlgorithmsModel } from "../models/alrogithms.model";

@Injectable()
export class AlgorithmsService {
    constructor() {}

    async handleAlgorithmLogic(
        query: AlgorithmsModel.AlgorithmsQuery,
        data: AlgorithmsModel.dataDTO[]
    ): Promise<AlgorithmsModel.dataResponseDTO> {
        
        // CALCULATE TOTAL SUM OF NOMINAL AMOUNT OF ALL OBJECTS
        const total = data.reduce((accumulator, object) => accumulator + object.nominalAmount, 0);
        
        // SORT DATA
        data.sort((a,b) => a.nominalAmount - b.nominalAmount);

        // CALCULATE TOTAL MAX CAPACITY OF BASKET 1 BASED ON NJ QUERY PARAMS
        const basket1AmountMax = total * query.nj / 100;
        console.log("BASKET 1 CAPACITY", basket1AmountMax)

        // DEFINE RESPONSE OBJECT
        const response: AlgorithmsModel.dataResponseDTO = new AlgorithmsModel.dataResponseDTO();

        response.basket1 = {
            basketAmountSum: 0,
            basketPercentageOfAll: 0,
            elements: []
        }
        response.basket2 = {
            basketAmountSum: 0,
            basketPercentageOfAll: 0,
            elements: []
        }

        // USING WHILE LOOP, WHICH WILL ALLOW US TO ITERATE ONLY AS MANY TIMES AS REQUIRED TO FILL MAXIMUM BUCKET 1 CAPACITY
        let basket1Index = 0;
        while (basket1Index < data.length && response.basket1.basketAmountSum + data[basket1Index].nominalAmount <= basket1AmountMax) {
            response.basket1.basketAmountSum += data[basket1Index].nominalAmount;
            basket1Index++;
        }

        // CHOOSING DATA FOR EACH BUCKET BASING ON RESULTS FROM WHILE LOOP
        response.basket1.elements = data.slice(0,basket1Index) 
        response.basket2.elements = data.slice(basket1Index) 
    
        // UPDATE BASKETS SUMS AND CALCULATE % OF DATA FOR EACH BUCKET BASED ON NUMBER OF ELEMENTS ASSIGNED TO SPECIFIC BUCKET IN RELATION TO THE TOTAL NUMBER OF ELEMENTS PASSED AS INPUT ARRAY
        Object.keys(response).map((el)=>{
            response[el].basketAmountSum = response[el].elements.reduce((accumulator, object) => accumulator + object.nominalAmount, 0);
            response[el].basketPercentageOfAll = (response[el].elements.length / data.length * 100).toFixed(3)
        })

        return response;
    }
}