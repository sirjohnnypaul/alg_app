
import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BUSINESS_LAYER_MODULES } from './business-layer';
import { ConfigModule } from '@nestjs/config';
import { LoadBodyMiddleware } from './middlewares/load-body/load-body.middleware';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: `./environments/${process.env.NODE_ENV || ''}.env`,
    }),
    ...BUSINESS_LAYER_MODULES
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  constructor() {}

  configure(consumer: MiddlewareConsumer) {
    consumer.apply(LoadBodyMiddleware).forRoutes({
      path: 'algorithms/',
      method: RequestMethod.GET,
    });
  }
}
