import { Injectable, NestMiddleware } from '@nestjs/common';
import * as fs from 'fs';
import * as path from 'path';

@Injectable()
export class LoadBodyMiddleware implements NestMiddleware {
  use(req: any, res: any, next: () => void) {
    console.log("PRE-LOADING DATA STARTED...")
    const data = fs.readFileSync(path.resolve(__dirname, '../../../assets/files/dataFile.json'), 'utf-8');
    req.body = JSON.parse(data);
    console.log("PRE-LOADING DATA DONE...")    
    next();
  }
}