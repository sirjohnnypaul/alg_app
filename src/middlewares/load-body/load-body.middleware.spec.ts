import { LoadBodyMiddleware } from "./load-body.middleware";

describe('LoadBodyInterceptor', () => {
  it('should be defined', () => {
    expect(new LoadBodyMiddleware()).toBeDefined();
  });
});
