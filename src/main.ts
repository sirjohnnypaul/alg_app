import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NestExpressApplication } from '@nestjs/platform-express';
import { BadRequestException, ValidationError, ValidationPipe } from '@nestjs/common';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule, {
    cors: true,
  });

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true
    }),
  );
  
  const port: number = process.env.APP_PORT ? +process.env.APP_PORT : 3000;
  await app.listen(port);
  console.log(`App launched on port ${port}`);
}
bootstrap();
